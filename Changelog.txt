FFE COOP Modpack Changelog
==========================
Mods in Pack
==========================

AGM - Minus AI PBO
ASDG_Jointrails
ASR AI
Blast Core
Blast Core Tracers
LHM_Para
Mao_Blood_Mist
Mocap
ST Hud
ST Stamina Bar
TMR Scopes
Xeno Taru Pod

=========================
V2.4
=========================

Rename to COOP_Modpack_ST
Updated Blast Core to Phoenix Beta 1
Updated Blastcore Tracers to Phoenix Beta 1
Added Xeno Taru Pod V1.3
Added ASDG_Jointrails V0.11
Added Mocap V1.0
Updated ASR AI to V0.9.13
Removed Rav Lifter - now redundant due to Arma Sling load & AGM Fast rope
Changed KNG.paa to FFE.paa with new logo
Updated Xeno Taru Pod V1.4

=========================
V2.3
=========================

Updated AGM to V0.94.1
Updated ASR AI to V0.9.11
Removed CBA from Modpack

=======================
Changelog Created V2.3
=======================