/*
 ASR AI3 settings
 this file must be found in <game folder>\userconfig\asr_ai3\
 for most settings, 0 = disabled, 1 = enabled

 All config entries are turned into global variables following a standard naming scheme. For example:
 asr_ai3_sysdanger_radiorange = getNumber (configFile >> "asr_ai3" >> "sysdanger" >> "radiorange")
 Mission makers can control these features by setting these global variables in init.sqf
*/

#define __FACTION_COEF(f,c) class ##f { coef = ##c; }

class sysdanger {
	enabled = 1;                   // All the other settings of this class matter only if we have 1 here
	debug = 0;                     // Log extra debugging info to RPT, create debug markers and hints (1-enabled, 0-disabled)
	radionet = 1;                  // AI groups share known enemy locations over radio
	radiorange = 500;              // Maximum range for AI radios
	seekcover = 1;                 // Set to 1 if AI should move to near cover in combat when they're exposed in the open, 0 to disable.
	throwsmoke = 0.4;              // AI throws smoke when advancing to cover (set 0 to disable or a number up to 1 to enable, higher number means better chance to do it)
	reactions[] = {1,1,1};         // Infantry groups will randomly react with basic, scripted, random actions, to detecting the enemy or being shot at;
	                               // format: {enableAttack,enableDefend,enableSupport}
	usebuildings = 0.8;            // Chance the AI group will enter nearby buildings when in combat mode (0 to 1 values, 0 will disable the feature)
};

class sysaiskill {
	enabled = 1;                   // All the other settings of this class matter only if we have 1 here
	debug = 0;                     // Log extra debugging info to RPT, create debug markers and hints (1-enabled, 0-disabled)
	setskills = 2;                 // Override AI skills based on their unit type (faction, training etc.; 0 - disabled, 1 - server only, 2 - setskill everywhere)
	dynsvd = 1;                    // Enable dynamic view distance adjustment based on day/night and fog thickness on dedicated servers and headless clients
	gunshothearing = 0.5;          // Gunshot hearing range coefficient (applied to shooter's weapon sound range; 0 will disable the feature)
	joinlast = 2;                  // Groups left with only this number of units will merge with nearest group of the same faction (set to 0 to disable)
	removegimps = 300;             // Units unable to walk for this time will separate from their group to prevent slowing it down (time in seconds, set 0 to disable)

	/*
	Units are classified into skill sets between 1 and 10
	By default, a lower level number means a better skilled unit
	Levels 8-10 are special:
	 - 8-9 are for pilots
	 - 10 is for trained snipers
	*/
	class sets {
		// only classes of arrays under this
		// skilltype = {<min value>, <random value added to min>};
		class level_0 { // super-AI (only used for testing)
			aiming[]   = {1.00, 0.00};	spotting[] = {1.00, 0.00};	general[]  = {1.00, 0.00};
			units[] = {}; // add class names to this to override their default (or inherited) skill set
		};
		class level_1 { // sf 1
			aiming[]   = {0.50, 0.10};	spotting[] = {0.40, 0.20};	general[]  = {0.80, 0.20};
			units[] = {};
		};
		class level_2 { // sf 2 (recon units, divers and spotters)
			aiming[]   = {0.45, 0.10};	spotting[] = {0.35, 0.20};	general[]  = {0.75, 0.20};
			units[] = {};
		};
		class level_3 { // regular 1 (regular army leaders, marksmen)
			aiming[]   = {0.40, 0.10};	spotting[] = {0.30, 0.20};	general[]  = {0.70, 0.20};
			units[] = {};
		};
		class level_4 { // regular 2 (regulars)
			aiming[]   = {0.35, 0.10};	spotting[] = {0.25, 0.20};	general[]  = {0.65, 0.20};
			units[] = {};
		};
		class level_5 { // militia or trained insurgents, former regulars (insurgent leaders, marksmen)
			aiming[]   = {0.30, 0.10};	spotting[] = {0.20, 0.20};	general[]  = {0.60, 0.20};
			units[] = {};
		};
		class level_6 { // civilians with some military training (insurgents)
			aiming[]   = {0.25, 0.10};	spotting[] = {0.15, 0.20};	general[]  = {0.55, 0.20};
			units[] = {};
		};
		class level_7 { // civilians without military training
			aiming[]   = {0.20, 0.10};	spotting[] = {0.10, 0.20};	general[]  = {0.50, 0.20};
			units[] = {};
		};
		class level_8 { // pilot 1 (regular)
			aiming[]   = {0.40, 0.10};	spotting[] = {0.50, 0.20};	general[]  = {0.70, 0.20};
			units[] = {};
		};
		class level_9 { // pilot 2 (insurgent)
			aiming[]   = {0.30, 0.10};	spotting[] = {0.40, 0.20};	general[]  = {0.60, 0.20};
			units[] = {};
		};
		class level_10 { // sniper
			aiming[]   = {0.60, 0.10};	spotting[] = {0.80, 0.20};	general[]  = {0.80, 0.20};
			units[] = {};
		};
	};//sets
	
	// apply skill coefficient by faction to the skills the units would get based on their skill levels and settings above
	// if a faction is missing the coefficient is assumed to be 1 by default
	// so no need to add any here unless you want custom faction coefficients
	class factions {

		__FACTION_COEF(BLU_F,1.0);	// A3 BLUFOR
		__FACTION_COEF(OPF_F,0.8);	// A3 OPFOR
		__FACTION_COEF(IND_F,0.8);	// A3 Greek Army
		__FACTION_COEF(IND_G_F,0.8);	// A3 Guerrilla

		__FACTION_COEF(USMC,1.0);	// A2 US Marine Corps
		__FACTION_COEF(CDF,1.0);	// A2 Chernarussian Defence Forces
		__FACTION_COEF(RU,1.0);		// A2 Russia
		__FACTION_COEF(INS,1.0);	// A2 Chedaki Insurgents
		__FACTION_COEF(GUE,1.0);	// A2 NAPA Guerilla
		__FACTION_COEF(BIS_TK,1.0);	// A2 Takistani Army
		__FACTION_COEF(BIS_TK_INS,1.0);	// A2 Takistani Insurgents
		__FACTION_COEF(BIS_US,1.0);	// A2 US Army
		__FACTION_COEF(BIS_CZ,1.0);	// A2 Czech
		__FACTION_COEF(BIS_GER,1.0);	// A2 Germany
		__FACTION_COEF(BIS_TK_GUE,1.0);	// A2 Takistani Guerilla
		__FACTION_COEF(BIS_UN,1.0);	// A2 UN
		__FACTION_COEF(PMC_BAF,1.0);	// A2 Private military
		__FACTION_COEF(BIS_BAF,1.0);	// A2 UK

	};//factions

};//skillsets

class sysgear {
	enabled = 1;                   // All the other settings of this class matter only if we have 1 here
	packNVG = 1;                   // Automatically un-equip NVG during the day (store them in the vest/backpack) and re-equip when it gets dark
	dayscope = 1;                  // Prevent AI from using scope fire modes at night (reduced engagement range and accuracy); requires ASDG JR to work
	debug = 0;                     // Log extra debugging info to RPT (1-enabled, 0-disabled)
};

version = 7; // will increment this when structure changes
